package mx.unam.diplomado.modelo.entidades;

import java.util.Objects;

import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity //definir la entidad
@Table(name = "t_tipo_computadora") //indicar el nombre de la tabla que representa
public class TipoContacto {
	
	@Id//indicar que la propiedad es una llave
	@Column(name = "id_computadora")
	
	private String id;
	@Column(name = "tipo_equipo")
	private String equipo;
	@Column(name = "tipo_fabricante")
	private String fabricante;
	@Column(name = "tipo_clave")
	private String clave;
	@Column(name = "tipo_existencias")
	private Integer existencias;
	


	public String getId() {
		return id;
	}
	@GeneratedValue(strategy = GenerationType.IDENTITY) //estrategia de manejo de llave
	@OneToOne(mappedBy="computadora", fetch = FetchType.LAZY)
	public void setId(String id) {
		this.id = id;
	}

	public String getEquipo() {
		return equipo;
	}

	public void setEquipo(String equipo) {
		this.equipo = equipo;
	}

	public String getFabricante() {
		return fabricante;
	}

	public void setFabricante(String fabricante) {
		this.fabricante = fabricante;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public Integer getExistencias() {
		return existencias;
	}

	public void setExistencias(Integer existencias) {
		this.existencias = existencias;
	}

	@Override
	public String toString() {
		return "TipoContacto [idComputadora=" + id + ", equipo=" + equipo + ", fabricante=" + fabricante
				+ ", clave=" + clave + ", existencias=" + existencias + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((clave == null) ? 0 : clave.hashCode());
		result = prime * result + ((equipo == null) ? 0 : equipo.hashCode());
		result = prime * result + ((existencias == null) ? 0 : existencias.hashCode());
		result = prime * result + ((fabricante == null) ? 0 : fabricante.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoContacto other = (TipoContacto) obj;
		if (clave == null) {
			if (other.clave != null)
				return false;
		} else if (!clave.equals(other.clave))
			return false;
		if (equipo == null) {
			if (other.equipo != null)
				return false;
		} else if (!equipo.equals(other.equipo))
			return false;
		if (existencias == null) {
			if (other.existencias != null)
				return false;
		} else if (!existencias.equals(other.existencias))
			return false;
		if (fabricante == null) {
			if (other.fabricante != null)
				return false;
		} else if (!fabricante.equals(other.fabricante))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}


}

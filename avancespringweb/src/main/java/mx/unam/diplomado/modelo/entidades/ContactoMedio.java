package mx.unam.diplomado.modelo.entidades;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "t_cobro")
public class ContactoMedio {
	
	
	@Id//indicar que la propiedad es una llave	
	@Column(name = "id_cobro")
	@GeneratedValue(strategy = GenerationType.IDENTITY) //estrategia de manejo de llave
	private Integer id;
	
	@Column(name = "id_cliente")
	private Integer idCliente;
	
	@Column(name = "id_precio")
	private Integer idPrecio;
	@Column(name = "cob_cantidad_articulos")
	private Integer cantidad;
	@Column(name = "cob_monto_total")
	private Float total;
	
	
	
//	private Integer id;
//	private String valor;
//	private Contacto contacto;
//	private MedioContacto medioContacto;
	
public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}

	public Integer getIdPrecio() {
		return idPrecio;
	}
	@OneToMany
	@JoinColumn(name = "id_precio", nullable = false)
	public void setIdPrecio(Integer idPrecio) {
		this.idPrecio = idPrecio;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public Float getTotal() {
		return total;
	}

	public void setTotal(Float total) {
		this.total = total;
	}
	
	
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cantidad == null) ? 0 : cantidad.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((idCliente == null) ? 0 : idCliente.hashCode());
		result = prime * result + ((idPrecio == null) ? 0 : idPrecio.hashCode());
		result = prime * result + ((total == null) ? 0 : total.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContactoMedio other = (ContactoMedio) obj;
		if (cantidad == null) {
			if (other.cantidad != null)
				return false;
		} else if (!cantidad.equals(other.cantidad))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (idCliente == null) {
			if (other.idCliente != null)
				return false;
		} else if (!idCliente.equals(other.idCliente))
			return false;
		if (idPrecio == null) {
			if (other.idPrecio != null)
				return false;
		} else if (!idPrecio.equals(other.idPrecio))
			return false;
		if (total == null) {
			if (other.total != null)
				return false;
		} else if (!total.equals(other.total))
			return false;
		return true;
	}	
	

	@Override
	public String toString() {
		return "ContactoMedio [id=" + id + ", idCliente=" + idCliente + ", idPrecio=" + idPrecio + ", cantidad="
				+ cantidad + ", total=" + total + "]";
	}

//	@JsonIgnore
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "id_contacto")
//	public Contacto getContacto() {
//		return contacto;
//	}
	
//	public void setContacto(Contacto contacto) {
//		this.contacto = contacto;
//	}
	
//	@ManyToOne(targetEntity = MedioContacto.class, fetch = FetchType.EAGER, optional = false)
//	@JoinColumn(name = "id_medio_contacto", nullable = false, referencedColumnName = "id_medio_contacto")
//	public MedioContacto getMedioContacto() {
//		return medioContacto;
//	}
	
//	public void setMedioContacto(MedioContacto medioContacto) {
//		this.medioContacto = medioContacto;
//	}


	
	

}


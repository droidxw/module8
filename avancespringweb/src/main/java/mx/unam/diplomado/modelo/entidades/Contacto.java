package mx.unam.diplomado.modelo.entidades;

import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;



@Entity
@Table(name = "t_precio_computadora")
@NamedQueries(
		{@NamedQuery(name = "contactosAll", query = "FROM Contacto"),
//		@NamedQuery(name = "contactoById", query = "SELECT c FROM Contacto c WHERE c.id = :id"),
//		@NamedQuery(name ="contactosByTipo", query = "SELECT c FROM Contacto c WHERE c.tipoContacto.id = :idTipoContacto")
		})
public class Contacto {
	
	@Id//indicar que la propiedad es una llave
	@Column(name = "id_precio")
	@GeneratedValue(strategy = GenerationType.IDENTITY) //estrategia de manejo de llave
	private Integer id;
	@Column(name = "id_computadora")
	private Integer computadora;
	@Column(name = "prec_computadora")
	private Float precio;
	@ManyToOne(targetEntity = ContactoMedio.class, optional = false, fetch = FetchType.LAZY) //indica el tipo de clase
//	@OneToMany(mappedBy = "contacto", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@Fetch(FetchMode.JOIN)
	private Set<TipoContacto> tipoContactos;
	
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	

	public Integer getComputadora() {
		return computadora;
	}
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_computadora", nullable = false)
	public void setComputadora(Integer computadora) {
		this.computadora = computadora;
	}

	public Float getPrecio() {
		return precio;
	}
//	@ManyToOne(targetEntity = TipoContacto.class, fetch = FetchType.LAZY, optional = false) 
	public void setPrecio(Float precio) {
		this.precio = precio;
	}
	

	public Set<TipoContacto> getTipoContactos() {
		return tipoContactos;
	}

	public void setTipoContactos(Set<TipoContacto> tipoContactos) {
		this.tipoContactos = tipoContactos;
	}

	@Override
	public String toString() {
		return "Contacto [id=" + id + ", computadora=" + computadora + ", precio=" + precio + ", tipoContactos="
				+ tipoContactos + "]";
	}
	
//	@OneToMany(mappedBy = "contacto", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
//	@Fetch(FetchMode.JOIN)
//	public Set<TipoContacto> getContactosMedios() {
//		return tipoContactos;
//	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((computadora == null) ? 0 : computadora.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((precio == null) ? 0 : precio.hashCode());
		result = prime * result + ((tipoContactos == null) ? 0 : tipoContactos.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Contacto other = (Contacto) obj;
		if (computadora == null) {
			if (other.computadora != null)
				return false;
		} else if (!computadora.equals(other.computadora))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (precio == null) {
			if (other.precio != null)
				return false;
		} else if (!precio.equals(other.precio))
			return false;
		if (tipoContactos == null) {
			if (other.tipoContactos != null)
				return false;
		} else if (!tipoContactos.equals(other.tipoContactos))
			return false;
		return true;
	}




}

package mx.unam.diplomado.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

//(clase a usar en vez de la configuracion xml en web.xmlxml)
public class AppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
//carga de clases de configuracion presentes en el framework (JPA)
	//puedo o no ir vacia
	//inyeccion clase PersistenceJPAConfig
	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class[] { PersistenceJPAConfig.class };
	}
//clase de configuracion mvc spring web
	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class[] { MvcConfiguration.class };
	}
//url (ruta a partir de la cual) para escuchar el servlet (raíz de mi aplicacion) 
	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}

}

package mx.unam.diplomado.form;

public class ContactoForm {

	private String nombre;
	private String apellidos;
	private Integer edad;
	private String direccion;
	private String tipoContacto;
	private String medioContacto;
	private String medio;
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public Integer getEdad() {
		return edad;
	}
	public void setEdad(Integer edad) {
		this.edad = edad;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTipoContacto() {
		return tipoContacto;
	}
	public void setTipoContacto(String tipoContacto) {
		this.tipoContacto = tipoContacto;
	}
	public String getMedioContacto() {
		return medioContacto;
	}
	public void setMedioContacto(String medioContacto) {
		this.medioContacto = medioContacto;
	}
	public String getMedio() {
		return medio;
	}
	public void setMedio(String medio) {
		this.medio = medio;
	}
	
	
}

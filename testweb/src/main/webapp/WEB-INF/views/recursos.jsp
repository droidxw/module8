<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link href="<c:url value="/resources/style.css"/>" type="text/css"  rel="stylesheet"/>
<title>Insert title here</title>
</head>
<body>
<h1>Ejemplo de recursos est&aacute;ticos</h1>

<img src="<c:url value="/resources/images/spring.jpg"/>"
alt ="imagen Spring" align="middle" width="600" height="350"/>

</body>
</html>